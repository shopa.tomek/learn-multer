
// https://medium.com/swlh/how-to-upload-image-using-multer-in-node-js-f3aeffb90657

var express = require('express')
var multer  = require('multer')

const mongoose = require('mongoose');
var mongoDB = 'mongodb+srv://tomasz:tomasz@cluster0.mwnam.mongodb.net/?retryWrites=true&w=majority';
const mongoURI = '127.0.0.1'; 
const connection = mongoose.createConnection(mongoDB); 
connection.once("open", () => { console.log("Connection Successful"); });
const fs = require('fs');


// console.log(process)

var app = express()

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, './uploads')
    },
    filename: function (req, file, cb) {
      cb(null, file.originalname)
    }
})
var upload = multer({ storage: storage })

app.use(express.static(__dirname + '/public'));
app.use('/uploads', express.static('uploads'));

app.post('/profile-upload-single', upload.single('profile-file'), function (req, res, next) {
  // req.file is the `profile-file` file
  // req.body will hold the text fields, if there were any
  console.log(JSON.stringify(req.file))
  var response = '<a href="/">Home</a><br>'
  response += "Files uploaded successfully.<br>"
  response += `<img src="${req.file.path}" /><br>`
  return res.send(response)
})

app.post('/profile-upload-multiple', upload.array('profile-files', 12), function (req, res, next) {
    // req.files is array of `profile-files` files
    // req.body will contain the text fields, if there were any
    var response = '<a href="/">Home</a><br>'
    response += "Files uploaded successfully.<br>"
    for(var i=0;i<req.files.length;i++){
        response += `<img src="${req.files[i].path}" /><br>`
    }
    
    return res.send(response)
})
   
let filenames = fs.readdirSync('uploads');
  
console.log("\nFilenames in directory:");
filenames.forEach((file) => {
    console.log("File:", file);
});
console.log(filenames)
var port = 3000;
app.listen(port,() => console.log(`Server http://localhost:${port}`))
